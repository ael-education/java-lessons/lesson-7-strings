/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package ru.ael.javastrings;

/**
 *
 * @author developer
 */
public class JavaStrings {

    public static void main(String[] args) {

        // Unicode  - кодировка символов в Java
        // Нумерация символов в строке начинается с 0
        String s = "Hello";
        //  Student st 

        System.out.println("Исследование строки: " + s);
        int len = s.length();
        System.out.println("Размер переменной s = [" + len + "] символов");

        char char0 = s.charAt(0);
        char char1 = s.charAt(1);
        char char2 = s.charAt(2);
        System.out.println("Символ с индексом 0 =  [" + char0 + "]");
        System.out.println("Символ с индексом 1 =  [" + char1 + "]");
        System.out.println("Символ с индексом 2 =  [" + char2 + "]");

        int countFoLetterL = 0;
        int countFoLetterH = 0;

        // Обход строки символ за символом
        for (int k = 0; k < s.length(); k++) { // Начало цикла

            char currentChar = s.charAt(k);
            System.out.println("переменная цикла [" + k + "] символ строки = " + currentChar);

            if (currentChar == 'l') {
                countFoLetterL++;
            }
            if (currentChar == 'H') {
                countFoLetterH++;
            }
        } // окончание цикла

        System.out.println("Количество букв l в слове: " + s + " [" + countFoLetterL + "] символов");
        System.out.println("Количество букв H в слове: " + s + " [" + countFoLetterH + "] символов");

        System.out.println("Исследование подстрок: ");
        String sub = s.substring(0, 3);
        System.out.println("Подстрока [0,3] строки: " + s + "  = " + sub);

        System.out.println("Изменение строк: ");
        String sNew = sub + "p";

        System.out.println("Результат добавления символа p к строке sub: " + sNew);

        System.out.println("Проверка совпадения строк");

        if (s.equals("Hello")) {
            System.out.println("Входная строка: " + s + " ответ: ДОБРЫЙ ДЕНЬ");
        }

        if (s.equals("Help")) {
            System.out.println("Входная строка: " + s + " ответ: ТРЕБУЕТСЯ ПОМОЩЬ");
        }

        if (sNew.equals("Help")) {
            System.out.println("Входная строка: " + sNew + " ответ: ТРЕБУЕТСЯ ПОМОЩЬ");
        }

        if (sNew.equalsIgnoreCase("help")) {
            System.out.println("Входная строка: " + sNew + " ответ без учета регистра: ТРЕБУЕТСЯ ПОМОЩЬ ");
        }
        
        
        

    }
}
